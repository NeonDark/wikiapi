BINARY_NAME:=wikiapi
DOCKER_REGISTRY:=bluedark/
LATEST_GIT_TAG = $(shell git describe --tags --abbrev=0)

GREEN  = $(shell tput -Txterm setaf 2)
YELLOW = $(shell tput -Txterm setaf 3)
WHITE  = $(shell tput -Txterm setaf 7)
CYAN   = $(shell tput -Txterm setaf 6)
RESET  = $(shell tput -Txterm sgr0)

.PHONY: all test build docker-build docker-release help vet update_dependencies tidy lint
.PHONY: openapi-fmt openapi-spec openapi-client openapi-publish push-tag

all: help

## Build:
build: ## Build your project and put the output binary in out/bin/
	mkdir -p build
	go build -o build/$(BINARY_NAME) ./cmd

clean: ## Remove build related file
	rm -rf ./build

## Test:
test: ## Run the tests of the project
	go test -cover -race ./...

vet: ## Run the tests of the project
	go vet ./...

coverage: ## Run the tests of the project and export the coverage
	go test -cover -covermode=count -coverprofile=profile.cov ./...
	go tool cover -func profile.cov

lint:
	golangci-lint run

## Docker:
docker-build: ## Use the dockerfile to build the container
	docker build --rm --tag $(DOCKER_REGISTRY)$(BINARY_NAME):latest .
	docker tag $(DOCKER_REGISTRY)$(BINARY_NAME) $(DOCKER_REGISTRY)$(BINARY_NAME):$(LATEST_GIT_TAG)

docker-release: docker-build ## Build and release the container with tag latest and version
	docker push $(DOCKER_REGISTRY)$(BINARY_NAME):latest
	docker push $(DOCKER_REGISTRY)$(BINARY_NAME):$(LATEST_GIT_TAG)

## Dependencies:
update_dependencies:
	go get -u ./...

tidy: ## Run go mod tidy on the default go.mod file
	go mod tidy

## Open API:
openapi-fmt: ## Formats swagger annotations
	./swag fmt -d cmd,server

openapi-spec: ## Creates openapi spec
	mkdir -p openapi && ./swag init --pd -d cmd,server -o openapi

openapi-client: ## Creates a javascript client based on the openapi spec
	(sudo docker compose -f docker-compose-openapi.yml up && sudo docker compose -f docker-compose-openapi.yml rm -fsv)

openapi-publish: ## Publishes an existing client to the configured repository
	(cd openapi/client/typescript-fetch && yarn npm publish)

## Git:
push-tag: ## increment the most recent tag and push it and any local commits
	bash createTag.sh

## Help:
help: ## Show this help.
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make${RESET} ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk 'BEGIN {FS = ":.*?## "} { \
		if (/^[a-zA-Z_-]+:.*?##.*$$/) {printf "    ${YELLOW}%-20s${GREEN}%s${RESET}\n", $$1, $$2} \
		else if (/^## .*$$/) {printf "  ${CYAN}%s${RESET}\n", substr($$1,4)} \
		}' $(MAKEFILE_LIST)