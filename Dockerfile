FROM golang:latest AS build-stage

ENV CGO_ENABLED=0

# download dependencies before build, so they can be cached
COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .
RUN go build -o /wikiapi ./cmd

FROM scratch
COPY --from=build-stage /wikiapi .

VOLUME ["/data"]
CMD ["--config", "/config.yml"]
ENTRYPOINT ["/wikiapi"]

EXPOSE 4567
