package server

import (
	"encoding/json"
	mw "github.com/qrest/gomisc/middleware"
	"github.com/qrest/gomisc/serror"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

// sendReply encodes the given reply into JSON and sends it
func sendReply(w http.ResponseWriter, reply any, statusCode int) {
	// use marshalling instead of encoding (streaming), as it gives better error handling
	// and because encoding buffers all data before writing: https://github.com/golang/go/issues/7872
	// todo check if https://github.com/golang/go/discussions/63397 has been accepted, merged and released and then rework json handling.
	replyBuffer, err := json.Marshal(reply)
	if err != nil {
		http.Error(w, "encoding error", http.StatusInternalServerError)
		warn(err)
		return
	}

	if reply == "" {
		w.Header().Set("Content-Type", "text/plain")
	} else {
		w.Header().Set("Content-Type", "application/json")
	}

	if statusCode == 0 {
		statusCode = http.StatusOK
	}
	w.WriteHeader(statusCode)

	if _, err := w.Write(replyBuffer); err != nil {
		// not possible to send response to client, so just log error
		warn(err)
	}
}

// setCacheHeader sets the client side caching to a third of the server side cache
func setCacheHeader(w http.ResponseWriter, duration time.Duration) {
	if duration == time.Duration(0) {
		duration = time.Hour * 24
	}
	w.Header().Set("Cache-Control", "max-age="+strconv.FormatInt(int64(duration/time.Second/3), 10))
}

// getFilepath joins directory and filename. If path traversal is detected in the filename, an error is returned
func getFilepath(directory string, filename string) (string, error) {
	if filename == "" {
		return "", serror.FromStr("filename is empty")
	}

	cleanedFileName := strings.Trim(filename, "/")

	directoryRoot, err := os.OpenRoot(directory)
	if err != nil {
		return "", serror.New(err)
	}

	defer func(directoryRoot *os.Root) {
		if err := directoryRoot.Close(); err != nil {
			warn(err)
		}
	}(directoryRoot)

	// check if file can be opened
	f, err := directoryRoot.Open(cleanedFileName)
	if err != nil {
		return "", serror.New(err)
	}

	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			warn(err)
		}
	}(f)

	return filepath.Join(directory, cleanedFileName), nil
}

// Root godoc
//
//	@Summary	Returns a redirect message
//	@Tags		default
//	@Produce	json
//	@Success	200	{object}	server.msgReply
//	@Router		/ [get]
func (s *Server) handlerRoot() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		msg := msgReply{Msg: "try /index"}

		sendReply(w, msg, http.StatusOK)
	})
}

// Get Blurb godoc
//
//	@Summary	Get a blurb of specified file
//	@Tags		default
//	@Produce	json
//	@Param		fileName	path		string	true	"file name"
//	@Success	200			{object}	server.blurbReply
//	@Failure	400			{object}	server.blurbReply
//	@Failure	500			{object}	server.blurbReply
//	@Router		/blurb/{fileName} [get]
func (s *Server) handlerGetBlurb(fileFolder string, route string,
	linkPrefix string, resourcePrefix string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		reply, status := getGetBlurbReply(fileFolder, r.URL.Path[len(route):], linkPrefix, resourcePrefix)

		sendReply(w, reply, status)
	})
}

// Get Markdown File godoc
//
//	@Summary	Get the specified file converted to markdown
//	@Tags		default
//	@Produce	json
//	@Param		fileName	path		string	true	"file name"
//	@Success	200			{object}	server.htmlReply
//	@Failure	400			{object}	server.htmlReply
//	@Failure	500			{object}	server.htmlReply
//	@Router		/file/{fileName} [get]
func (s *Server) handlerGetFile(fileFolder string, route string,
	linkPrefix string, resourcePrefix string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		reply, status := getGetFileReply(fileFolder, r.URL.Path[len(route):], linkPrefix, resourcePrefix)

		sendReply(w, reply, status)
	})
}

// Get File godoc
//
//	@Summary	Get the specified file
//	@Tags		default
//	@Produce	text/plain
//	@Param		fileName	path		string	true	"file name"
//	@Success	200			{file}		file
//	@Failure	500			{string}	string	""
//	@Router		/res/{fileName} [get]
func (s *Server) handlerGetResource(fileFolder string, route string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		filePath, err := getFilepath(fileFolder, r.URL.Path[len(route):])
		if err != nil {
			http.Error(w, "", http.StatusInternalServerError)
			warn(err)
			return
		}

		http.ServeFile(w, r, filePath)
	})
}

// Index godoc
//
//	@Summary	Index of files
//	@Tags		default
//	@Produce	json
//	@Success	200	{object}	server.indexReply
//	@Failure	500	{object}	server.indexReply
//	@Router		/index/ [get]
func (s *Server) handlerIndex(rootFolder string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		reply, status := getIndexReply(rootFolder)
		sendReply(w, reply, status)
	})
}

// Search godoc
//
//	@Summary	Search files by text
//	@Tags		default
//	@Param		query	body	server.getSearchReply.request	true	"Search query"
//	@Produce	json
//	@Success	200	{object}	server.searchReply
//	@Failure	400	{object}	server.searchReply
//	@Failure	500	{object}	server.searchReply
//	@Router		/search/ [post]
func (s *Server) handlerSearch() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		reply, status := getSearchReply(s.searchIndex, r)
		sendReply(w, reply, status)
	})
}

func (s *Server) setupHandlers(fileFolder string, linkPrefix string, resourcePrefix string) {
	const cacheTTL = time.Minute * 30

	s.handler.Handle("GET /", mw.Adapt(s.handlerRoot(), s.cacheFactory(cacheTTL)))
	s.handler.Handle("GET /blurb/", mw.Adapt(s.handlerGetBlurb(fileFolder, "/blurb/", linkPrefix, resourcePrefix), s.cacheFactory(cacheTTL)))
	s.handler.Handle("GET /file/", mw.Adapt(s.handlerGetFile(fileFolder, "/file/", linkPrefix, resourcePrefix), s.cacheFactory(cacheTTL)))
	s.handler.Handle("GET /res/", mw.Adapt(s.handlerGetResource(fileFolder, "/res/"), s.cacheFactory(cacheTTL)))
	s.handler.Handle("GET /index/", mw.Adapt(s.handlerIndex(fileFolder), s.cacheFactory(cacheTTL)))
	s.handler.Handle("POST /search/", mw.Adapt(s.handlerSearch(), mw.MaxBody5MiB(), s.cacheFactory(cacheTTL)))
}
