package server

import (
	"encoding/json"
	mw "github.com/qrest/gomisc/middleware"
	"github.com/stretchr/testify/require"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

const testFileDir = "testResources"
const rootFileName = "index.md"
const subdir = "dir1"
const subdirFileName1 = subdir + "/file1.md"
const subdirFileName2 = subdir + "/file2.md"

type tests struct {
	fileName string
	success  bool
}

var cases = []tests{
	{
		fileName: rootFileName,
		success:  true,
	},
	{
		fileName: "dir1/file1.md",
		success:  true,
	},
	{
		fileName: subdirFileName1,
		success:  true,
	},
	{
		fileName: subdirFileName2,
		success:  true,
	},
	{
		fileName: "dir1/invalid_file_name",
		success:  false,
	},
	{
		fileName: "dir1/../dir1/file1.md",
		success:  true,
	},
	{
		fileName: "../api_test.go",
		success:  false,
	},
	{
		fileName: "",
		success:  false,
	},
}

func TestSetCacheHeader(t *testing.T) {
	recorder := httptest.NewRecorder()
	require.NotPanics(t, func() {
		setCacheHeader(recorder, time.Minute)
	})

	require.NotPanics(t, func() {
		setCacheHeader(recorder, time.Duration(0))
	})
}

func TestGetFilePath(t *testing.T) {
	for _, c := range cases {
		_, err := getFilepath(testFileDir, c.fileName)
		if c.success {
			require.NoError(t, err)
		} else {
			require.Error(t, err)
		}
	}
}

func NewServerWithoutCache() Server {
	factory, err := mw.NewCacheFactory(10, nil)
	if err != nil {
		panic(err)
	}

	return Server{cacheFactory: factory, handler: http.NewServeMux()}
}

func TestHandlerRoot(t *testing.T) {
	w := httptest.NewRecorder()
	srv := NewServerWithoutCache()
	srv.handlerRoot().ServeHTTP(w, nil)

	response := w.Result()
	defer response.Body.Close()
	var r msgReply
	err := json.NewDecoder(response.Body).Decode(&r)
	require.NoError(t, err)
	require.EqualValues(t, "try /index", r.Msg)
}

func TestHandlerGetBlurb(t *testing.T) {
	for _, c := range cases {
		req := httptest.NewRequest(http.MethodGet, "/blurb/"+c.fileName, nil)
		w := httptest.NewRecorder()
		srv := NewServerWithoutCache()
		srv.handlerGetBlurb(testFileDir, "/blurb/",
			"/linkPrefix/", "/resourcePrefix/").ServeHTTP(w, req)

		// wrap defer in function
		func() {
			response := w.Result()
			defer response.Body.Close()
			var r blurbReply
			err := json.NewDecoder(response.Body).Decode(&r)
			require.NoError(t, err)
			if c.success {
				require.Equal(t, http.StatusOK, response.StatusCode)
				require.NotEmpty(t, r.Blurb)
			} else {
				require.NotEqual(t, http.StatusOK, response.StatusCode)
			}
		}()
	}
}

func TestHandlerGetFile(t *testing.T) {
	for _, c := range cases {
		req := httptest.NewRequest(http.MethodGet, "/file/"+c.fileName, nil)
		w := httptest.NewRecorder()
		srv := NewServerWithoutCache()
		srv.handlerGetFile(testFileDir, "/file/",
			"/linkPrefix/", "/resourcePrefix/").ServeHTTP(w, req)

		// wrap defer in function
		func() {
			response := w.Result()
			defer response.Body.Close()
			var r htmlReply
			err := json.NewDecoder(response.Body).Decode(&r)
			require.NoError(t, err)
			if c.success {
				require.Equal(t, http.StatusOK, response.StatusCode)
				require.NotEmpty(t, r.HTML)
			} else {
				require.NotEqual(t, http.StatusOK, response.StatusCode)
			}
		}()
	}
}

func TestHandlerGetResource(t *testing.T) {
	for _, c := range cases {
		req := httptest.NewRequest(http.MethodGet, "/res/"+c.fileName, nil)
		w := httptest.NewRecorder()
		srv := NewServerWithoutCache()
		srv.handlerGetResource(testFileDir, "/res/").ServeHTTP(w, req)

		// wrap defer in function
		func() {
			response := w.Result()
			defer response.Body.Close()

			responseFile, err := io.ReadAll(response.Body)
			require.NoError(t, err)
			require.NotEmpty(t, responseFile)
		}()
	}
}

func TestHandlerIndex(t *testing.T) {
	w := httptest.NewRecorder()
	srv := NewServerWithoutCache()
	srv.handlerIndex(testFileDir).ServeHTTP(w, nil)

	response := w.Result()
	defer response.Body.Close()

	var r indexReply
	err := json.NewDecoder(response.Body).Decode(&r)
	require.NoError(t, err)
	require.NotEmpty(t, r.Files)
	require.Equal(t, http.StatusOK, response.StatusCode)
}

func TestSetupHandlers(t *testing.T) {
	srv, err := NewServer(1)
	require.NoError(t, err)

	require.NotPanics(t, func() {
		srv.setupHandlers(testFileDir, "/linkPrefix/", "/resPrefix/")
	})
}

func Test_sendReply(t *testing.T) {
	w := httptest.NewRecorder()

	type someType struct {
		Msg string `json:"msg,omitempty"`
	}
	r := someType{Msg: "test"}

	sendReply(w, r, http.StatusOK)

	// wrap defer in function
	func() {
		response := w.Result()
		defer response.Body.Close()

		responseFile, err := io.ReadAll(response.Body)
		require.NoError(t, err)
		require.NotEmpty(t, responseFile)
	}()

	sendReply(w, nil, http.StatusOK)

	// wrap defer in function
	func() {
		response := w.Result()
		defer response.Body.Close()

		responseFile, err := io.ReadAll(response.Body)
		require.NoError(t, err)
		require.Empty(t, responseFile)
	}()
}
