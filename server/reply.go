package server

import (
	"encoding/json"
	"github.com/blevesearch/bleve/v2"
	"github.com/qrest/gomisc/serror"
	"io"
	"io/fs"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/ast"
	"github.com/gomarkdown/markdown/html"
)

type indexReply struct {
	Files []string `json:"index,omitempty"`
}

type blurbReply struct {
	Blurb string `json:"blurb,omitempty"`
}

type SearchResult struct {
	Filename string `json:"filename,omitempty"`
	Fragment string `json:"fragment,omitempty"`
}

type searchReply struct {
	SearchResults []SearchResult `json:"searchResults,omitempty"`
}

type htmlReply struct {
	HTML string `json:"html,omitempty"`
}
type msgReply struct {
	Msg string `json:"msg,omitempty"`
}

func getFilesRecursively(rootFolder string) ([]string, error) {
	var files []string

	rootFolderStrLen := len(rootFolder) + 1

	walk := func(s string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		if !d.IsDir() && strings.HasSuffix(d.Name(), ".md") {
			files = append(files, s[rootFolderStrLen:])
		}
		return nil
	}

	if err := filepath.WalkDir(rootFolder, walk); err != nil {
		return nil, serror.New(err)
	}

	return files, nil
}

func getIndexReply(rootFolder string) (r indexReply, status int) {
	files, err := getFilesRecursively(rootFolder)
	if err != nil {
		status = http.StatusInternalServerError
		warn(err)
		return
	}

	r.Files = files
	return
}

// isAbsoluteDestination returns true if the given destination
// links to an absolute path (starts with http:// or https://)
func isAbsoluteDestination(dst []byte) bool {
	exclusionPrefix := []string{"http://", "https://"}
	destination := string(dst)

	for _, excludedString := range exclusionPrefix {
		if strings.HasPrefix(destination, excludedString) {
			return true
		}
	}
	return false
}

func convertFile(fileName string, getBlurb bool, linkPrefix string, resourcePrefix string) (string, error) {
	blurbBytes, err := os.ReadFile(fileName)
	if err != nil {
		return "", serror.New(err)
	}

	var foundHeading int
	var numLinksTraversed int
	var numImagesTraversed int

	imagePrefix := resourcePrefix + "res/"

	renderHookDropCodeBlock := func(_ io.Writer, node ast.Node, _ bool) (ast.WalkStatus, bool) {
		if getBlurb {
			if _, ok := node.(*ast.Heading); ok {
				if foundHeading == 2 {
					// found next heading -> blurb over
					return ast.Terminate, true
				}

				foundHeading++
			}
		}

		if v, ok := node.(*ast.Link); ok {
			if isAbsoluteDestination(v.Destination) {
				return ast.GoToNext, false
			}

			// link tags occur in pairs of two. the destination only needs to be changed for the first tag.
			numLinksTraversed++
			if numLinksTraversed%2 != 0 {
				v.Destination = append([]byte(linkPrefix), v.Destination...)
			}
		} else if v, ok := node.(*ast.Image); ok {
			if isAbsoluteDestination(v.Destination) {
				return ast.GoToNext, false
			}

			// image tags occur in pairs of two. the destination only needs to be changed for the first tag.
			numImagesTraversed++
			if numImagesTraversed%2 != 0 {
				v.Destination = append([]byte(imagePrefix), v.Destination...)
			}
		}

		return ast.GoToNext, false
	}

	opts := html.RendererOptions{
		Flags:          html.CommonFlags,
		RenderNodeHook: renderHookDropCodeBlock,
	}

	return string(markdown.ToHTML(blurbBytes, nil, html.NewRenderer(opts))), nil
}

func getGetFileReply(fileFolder string, fileName string, linkPrefix string, resourcePrefix string) (r htmlReply, status int) {
	filePath, err := getFilepath(fileFolder, fileName)
	if err != nil {
		status = http.StatusBadRequest
		warn(err)
		return
	}

	content, err := convertFile(filePath, false, linkPrefix, resourcePrefix)
	if err != nil {
		status = http.StatusInternalServerError
		warn(err)
		return
	}

	r.HTML = content
	return
}

func getGetBlurbReply(fileFolder string, fileName string, linkPrefix string, resourcePrefix string) (r blurbReply, status int) {
	filePath, err := getFilepath(fileFolder, fileName)
	if err != nil {
		status = http.StatusBadRequest
		warn(err)
		return
	}

	blurb, err := convertFile(filePath, true, linkPrefix, resourcePrefix)
	if err != nil {
		status = http.StatusInternalServerError
		warn(err)
		return
	}

	r.Blurb = blurb
	return
}

func getSearchReply(searchIndex bleve.Index, req *http.Request) (r searchReply, status int) {
	type request struct {
		Query string `json:"query"`
	}

	var searchRequest request
	if err := json.NewDecoder(req.Body).Decode(&searchRequest); err != nil {
		status = http.StatusBadRequest
		warn(serror.New(err))
		return
	}

	if searchRequest.Query == "" {
		status = http.StatusBadRequest
		return
	}

	search := bleve.NewSearchRequest(bleve.NewQueryStringQuery(searchRequest.Query))
	search.Highlight = bleve.NewHighlightWithStyle("html")

	searchResult, err := searchIndex.Search(search)
	if err != nil {
		status = http.StatusInternalServerError
		warn(serror.New(err))
		return
	}

	// if regular query did not return results try a fuzzy query
	if len(searchResult.Hits) == 0 {
		fuzz := bleve.NewFuzzyQuery(searchRequest.Query)
		fuzz.SetFuzziness(2)
		search = bleve.NewSearchRequest(fuzz)
		search.Highlight = bleve.NewHighlightWithStyle("html")
		searchResult, err = searchIndex.Search(search)
		if err != nil {
			status = http.StatusInternalServerError
			warn(serror.New(err))
			return
		}
	}

	if len(searchResult.Hits) == 0 {
		return
	}

	const maxResults = 5
	r.SearchResults = make([]SearchResult, min(maxResults, len(searchResult.Hits)))

	for i, hit := range searchResult.Hits {
		var frag string

		// get one fragment per hit
		for _, f := range hit.Fragments {
			if len(f) > 0 {
				frag = f[0]
				break
			}
		}

		r.SearchResults[i] = SearchResult{Filename: hit.ID, Fragment: frag}

		// limit to maxResults
		if i == maxResults-1 {
			break
		}
	}

	return
}
