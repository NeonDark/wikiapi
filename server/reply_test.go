package server

import (
	"github.com/stretchr/testify/require"
	"net/http"
	"testing"
)

func TestGetFilesRecursively(t *testing.T) {
	files, err := getFilesRecursively(testFileDir)
	require.NoError(t, err)
	require.Len(t, files, 3)

	// test empty folder
	_, err = getFilesRecursively("")
	require.Error(t, err)

	// test invalid folder
	_, err = getFilesRecursively("invalid_folder")
	require.Error(t, err)
}

func TestGetIndexReply(t *testing.T) {
	rpl, status := getIndexReply(testFileDir)
	require.Zero(t, status)
	require.Len(t, rpl.Files, 3)

	// test empty folder
	_, status = getIndexReply("")
	require.Equal(t, http.StatusInternalServerError, status)

	// test invalid folder
	_, status = getIndexReply("invalid_folder")
	require.Equal(t, http.StatusInternalServerError, status)
}

func TestIsAbsoluteDestination(t *testing.T) {
	require.False(t, isAbsoluteDestination(nil))
	require.False(t, isAbsoluteDestination([]byte("")))
	require.False(t, isAbsoluteDestination([]byte{}))
	require.False(t, isAbsoluteDestination([]byte("test123")))
	require.False(t, isAbsoluteDestination([]byte("test.comhttps://")))
	require.True(t, isAbsoluteDestination([]byte("http://test.com")))
	require.True(t, isAbsoluteDestination([]byte("https://test.com")))
}

func TestConvertFile(t *testing.T) {
	file, err := convertFile(testFileDir+"/"+rootFileName, false, "/linkPrefix/", "/resPrefix/")
	require.NoError(t, err)
	require.NotEmpty(t, file)

	file, err = convertFile(testFileDir+"/"+subdirFileName1, true, "/linkPrefix/", "/resPrefix/")
	require.NoError(t, err)
	require.NotEmpty(t, file)

	_, err = convertFile("", true, "/linkPrefix/", "/resPrefix/")
	require.Error(t, err)
}

func TestGetGetFileReply(t *testing.T) {
	rpl, status := getGetFileReply(testFileDir, subdirFileName1, "/linkPrefix/", "/resPrefix/")
	require.Zero(t, status)
	require.NotEmpty(t, rpl.HTML)

	rpl, status = getGetFileReply(testFileDir, subdirFileName2, "/linkPrefix/", "/resPrefix/")
	require.Zero(t, status)
	require.NotEmpty(t, rpl.HTML)

	_, status = getGetFileReply(testFileDir, "", "/linkPrefix/", "/resPrefix/")
	require.Equal(t, http.StatusBadRequest, status)

	_, status = getGetFileReply("", "", "", "")
	require.Equal(t, http.StatusBadRequest, status)
}

func TestGetGetBlurbReply(t *testing.T) {
	rpl, status := getGetBlurbReply(testFileDir, subdirFileName1, "/linkPrefix/", "/resPrefix/")
	require.Zero(t, status)
	require.NotEmpty(t, rpl.Blurb)

	rpl, status = getGetBlurbReply(testFileDir, subdirFileName2, "/linkPrefix/", "/resPrefix/")
	require.Zero(t, status)
	require.NotEmpty(t, rpl.Blurb)

	_, status = getGetBlurbReply(testFileDir, "", "/linkPrefix/", "/resPrefix/")
	require.Equal(t, http.StatusBadRequest, status)

	_, status = getGetBlurbReply("", "", "", "")
	require.Equal(t, http.StatusBadRequest, status)
}
