package server

import (
	"context"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	m.Run()
}

func TestNewServer(t *testing.T) {
	srv, err := NewServer(1)
	require.NoError(t, err)
	require.NotNil(t, srv.cacheFactory)
	require.NotNil(t, srv.handler)
}

func TestServer_StartServer(t *testing.T) {
	srv, err := NewServer(1)
	require.NoError(t, err)

	httpSrv := srv.StartServer(testFileDir, 20000, "/linkPrefix/", "/resPrefix/")
	require.NotNil(t, httpSrv)

	ctx, cancelFunc := context.WithTimeout(t.Context(), time.Second*3)
	defer cancelFunc()
	err = httpSrv.Shutdown(ctx)
	require.NoError(t, err)
}
