package server

import (
	"errors"
	"github.com/blevesearch/bleve/v2"
	"github.com/gomarkdown/markdown"
	mw "github.com/qrest/gomisc/middleware"
	"github.com/qrest/gomisc/serror"
	text "gitlab.com/NeonDark/wikiapi/text"
	"log/slog"
	"net/http"
	"os"
	"strconv"
	"time"
)

func warn(err error, v ...any) {
	serror.Log(slog.Default(), err, v...)
}

type Server struct {
	cacheFactory func(duration time.Duration) mw.Adapter
	handler      *http.ServeMux
	searchIndex  bleve.Index
}

func NewServer(maxCacheSize int64) (*Server, error) {
	factory, err := mw.NewCacheFactory(maxCacheSize, nil)
	if err != nil {
		return nil, err
	}
	mapping := bleve.NewIndexMapping()
	mapping.DefaultAnalyzer = "en"
	index, err := bleve.NewMemOnly(mapping)
	if err != nil {
		return nil, serror.New(err)
	}

	return &Server{
		cacheFactory: factory,
		handler:      http.NewServeMux(),
		searchIndex:  index,
	}, nil
}

func indexAllFiles(searchIndex bleve.Index, fileFolder string) error {
	filePaths, err := getFilesRecursively(fileFolder)
	if err != nil {
		return err
	}

	for _, filePath := range filePaths {
		blurbBytes, err := os.ReadFile(fileFolder + "/" + filePath)
		if err != nil {
			return serror.New(err)
		}

		data := map[string]string{
			"data": string(markdown.Render(markdown.Parse(blurbBytes, nil), text.NewRenderer())),
		}

		if err = searchIndex.Index(filePath, data); err != nil {
			return serror.New(err)
		}
	}
	return nil
}

func (s *Server) StartServer(fileFolder string, port int, linkPrefix string, resourcePrefix string) *http.Server {
	s.setupHandlers(fileFolder, linkPrefix, resourcePrefix)

	if err := indexAllFiles(s.searchIndex, fileFolder); err != nil {
		panic(err)
	}

	srv := &http.Server{
		Addr:              "0.0.0.0:" + strconv.Itoa(port),
		Handler:           s.handler,
		ReadHeaderTimeout: time.Second * 5,
		ReadTimeout:       time.Minute,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}()

	return srv
}
