package md

import (
	"fmt"
	"io"
	"strings"

	"github.com/gomarkdown/markdown/ast"
)

// Renderer renders to text
// Copied and adapted from github.com/gomarkdown/markdown/md/md_renderer.go.
type Renderer struct {
	orderedListCounter map[int]int
	// used to keep track of whether a given list item uses a paragraph
	// for large spacing.
	paragraph map[int]bool

	lastOutputLen  int
	listDepth      int
	indentSize     int
	lastNormalText string
}

// NewRenderer returns a text renderer.
func NewRenderer() *Renderer {
	return &Renderer{
		orderedListCounter: map[int]int{},
		paragraph:          map[int]bool{},
		indentSize:         4,
	}
}

func (r *Renderer) out(w io.Writer, d []byte) {
	r.lastOutputLen = len(d)
	_, _ = w.Write(d)
}

func (r *Renderer) outs(w io.Writer, s string) {
	r.lastOutputLen = len(s)
	_, _ = io.WriteString(w, s)
}

func (r *Renderer) doubleSpace(w io.Writer) {
	// TODO: need to remember number of written bytes
	//if out.Len() > 0 {
	r.outs(w, "\n")
	//}
}

func (r *Renderer) list(node *ast.List, entering bool) {
	if entering {
		r.listDepth++
		flags := node.ListFlags
		if flags&ast.ListTypeOrdered != 0 {
			r.orderedListCounter[r.listDepth] = 1
		}
	} else {
		r.listDepth--
	}
}

func (r *Renderer) listItem(w io.Writer, node *ast.ListItem, entering bool) {
	flags := node.ListFlags
	bullet := string(node.BulletChar)

	if entering {
		for i := 1; i < r.listDepth; i++ {
			for range r.indentSize {
				_, _ = fmt.Fprintf(w, " ")
			}
		}
		if flags&ast.ListTypeOrdered != 0 {
			_, _ = fmt.Fprintf(w, "%d. ", r.orderedListCounter[r.listDepth])
			r.orderedListCounter[r.listDepth]++
		} else {
			_, _ = fmt.Fprintf(w, "%s ", bullet)
		}
	}
}

func (r *Renderer) para(w io.Writer, entering bool) {
	if !entering && r.lastOutputLen > 0 {
		r.outs(w, "\n")
	}
}

func isNumber(data []byte) bool {
	for _, b := range data {
		if b < '0' || b > '9' {
			return false
		}
	}
	return true
}

func needsEscaping(text []byte, lastNormalText string) bool {
	switch string(text) {
	case `\`,
		"`",
		"*",
		"_",
		"{", "}",
		"[", "]",
		"(", ")",
		"#",
		"+",
		"-":
		return true
	case "!":
		return false
	case ".":
		// Return true if number, because a period after a number must be escaped to not get parsed as an ordered list.
		return isNumber([]byte(lastNormalText))
	case "<", ">":
		return true
	default:
		return false
	}
}

// cleanWithoutTrim is like clean, but doesn't trim blanks.
func cleanWithoutTrim(s string) string {
	var b []byte
	var p byte
	for i := range len(s) {
		q := s[i]
		if q == '\n' || q == '\r' || q == '\t' {
			q = ' '
		}
		if q != ' ' || p != ' ' {
			b = append(b, q)
			p = q
		}
	}
	return string(b)
}

func (r *Renderer) text(w io.Writer, text *ast.Text) {
	lit := text.Literal
	normalText := string(text.Literal)
	if needsEscaping(lit, r.lastNormalText) {
		lit = append([]byte("\\"), lit...)
	}
	r.lastNormalText = normalText
	if r.listDepth > 0 && string(lit) == "\n" {
		// TODO: See if this can be cleaned up... It's needed for lists.
		return
	}
	cleanString := cleanWithoutTrim(string(lit))
	if cleanString == "" {
		return
	}

	r.outs(w, cleanString)
	// If it ends with a space, make note of that.
	//if len(cleanString) >= 1 && cleanString[len(cleanString)-1] == ' ' {
	// TODO: write equivalent of this
	// r.normalTextMarker[out] = out.Len()
	//}
}

func (r *Renderer) htmlSpan(w io.Writer, node *ast.HTMLSpan) {
	r.out(w, node.Literal)
}

func (r *Renderer) htmlBlock(w io.Writer, node *ast.HTMLBlock) {
	r.doubleSpace(w)
	r.out(w, node.Literal)
	r.outs(w, "\n\n")
}

func (r *Renderer) codeBlock(w io.Writer, node *ast.CodeBlock) {
	r.doubleSpace(w)
	text := node.Literal
	lang := string(node.Info)
	// Parse out the language name.
	count := 0
	for _, elt := range strings.Fields(lang) {
		if elt[0] == '.' {
			elt = elt[1:]
		}
		if len(elt) == 0 {
			continue
		}
		r.outs(w, "```")
		r.outs(w, elt)
		count++
		break
	}

	if count == 0 {
		r.outs(w, "```")
	}
	r.outs(w, "\n")
	r.out(w, text)
	r.outs(w, "\n```\n")
}

func (r *Renderer) code(w io.Writer, node *ast.Code) {
	r.out(w, node.Literal)
}

func (r *Renderer) heading(w io.Writer, node *ast.Heading, entering bool) {
	if entering {
		r.out(w, node.Literal)
	} else {
		r.outs(w, "\n")
	}
}

// RenderNode renders text nodes
func (r *Renderer) RenderNode(w io.Writer, node ast.Node, entering bool) ast.WalkStatus { //nolint:gocyclo
	switch node := node.(type) {
	case *ast.Text:
		r.text(w, node)
	case *ast.Softbreak:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Hardbreak:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Emph: // do nothing
	case *ast.Strong: // do nothing
	case *ast.Del: // do nothing
	case *ast.BlockQuote:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Aside:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Link:
		// don't render the actual link, only the link title (done via case text)
	case *ast.CrossReference:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Citation:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Image:
		// do nothing
	case *ast.Code:
		r.code(w, node)
	case *ast.CodeBlock:
		r.codeBlock(w, node)
	case *ast.Caption:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.CaptionFigure:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Document:
		// do nothing
	case *ast.Paragraph:
		r.para(w, entering)
	case *ast.HTMLSpan:
		r.htmlSpan(w, node)
	case *ast.HTMLBlock:
		r.htmlBlock(w, node)
	case *ast.Heading:
		r.heading(w, node, entering)
	case *ast.HorizontalRule:
		// do nothing
	case *ast.List:
		r.list(node, entering)
	case *ast.ListItem:
		r.listItem(w, node, entering)
	case *ast.Table:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.TableCell:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.TableHeader:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.TableBody:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.TableRow:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.TableFooter:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Math:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.MathBlock:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.DocumentMatter:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Callout:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Index:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Subscript:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Superscript:
		panic(fmt.Sprintf("node %T NYI", node))
	case *ast.Footnotes:
		// nothing by default; just output the list.
	default:
		panic(fmt.Sprintf("Unknown node %T", node))
	}
	return ast.GoToNext
}

// RenderHeader renders header
func (r *Renderer) RenderHeader(_ io.Writer, _ ast.Node) {
	// do nothing
}

// RenderFooter renders footer
func (r *Renderer) RenderFooter(_ io.Writer, _ ast.Node) {
	// do nothing
}
