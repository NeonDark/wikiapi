package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/qrest/gomisc/config"
	"github.com/qrest/gomisc/serror"
	"gitlab.com/NeonDark/wikiapi/server"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func info(msg string, v ...any) {
	slog.Default().Info(msg, append([]any{"module", "main"}, v...)...)
}

func warn(err error, v ...any) {
	serror.Log(slog.Default(), err, v...)
}

type Config struct {
	Port       int    `yaml:"port"`
	Path       string `yaml:"path"`
	CacheSize  int    `yaml:"cacheSize"`
	LinkPrefix string `yaml:"linkPrefix"`
	APIPrefix  string `yaml:"apiPrefix"`
}

var defaultConfig = Config{
	Port:      4567,
	Path:      "",
	CacheSize: 256,
}

// @title			WikiAPI API
// @version		1.1.0
// @description	This is the REST API for WikiAPI
// @host			localhost:4567
func main() {
	////// SET FLAGS //////

	defaultConfigName := "config.yml"
	var filePath string
	var createConfigFile bool
	config.SetConfigFlags(defaultConfigName, &filePath, &createConfigFile)
	flag.Parse()

	////// CONFIGURATION FILE HANDLING //////

	if createConfigFile {
		fmt.Println("Generating configuration file ...")

		err := config.WriteConfig(defaultConfigName, defaultConfig)
		if err != nil {
			fmt.Println(err)
			return
		}

		fmt.Println("config file", defaultConfigName, "successfully created")
		return
	}

	var newConfig Config
	if err := config.ReadConfig(filePath, &newConfig); err != nil {
		fmt.Println(err)
		return
	}

	slog.SetDefault(slog.New(slog.NewJSONHandler(os.Stdout, nil)))

	if newConfig.Port < 1 || newConfig.Port > 65535 {
		warn(serror.FromFormat("invalid port: %d", newConfig.Port))
		return
	}

	if newConfig.Path == "" {
		warn(serror.FromStr("path is empty"))
		return
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	apiServer, err := server.NewServer(int64(newConfig.CacheSize))
	if err != nil {
		warn(err)
		return
	}

	// start server
	srv := apiServer.StartServer(newConfig.Path, newConfig.Port, newConfig.LinkPrefix, newConfig.APIPrefix)

	info("Serving folder", "port", newConfig.Port, "max cache size (MiB)",
		newConfig.CacheSize, "link prefix", newConfig.LinkPrefix, "API prefix", newConfig.APIPrefix)
	<-done
	info("Exiting ...")

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer func() {
		// extra handling here
		cancel()
	}()

	if shutDownError := srv.Shutdown(ctx); shutDownError != nil {
		warn(shutDownError)
		return
	}
}
